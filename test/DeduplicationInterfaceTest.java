
import org.junit.Assert;


public class DeduplicationInterfaceTest {

    DeduplicationInterface duplikcatInterface;

    @org.junit.Test
    public void addSentence1() { ;

        duplikcatInterface.addSentence("Ala ma kota");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(), 1);
    }

    @org.junit.Test
    public void checkException2() {

        Boolean isExcetpion = false;
        try{
            duplikcatInterface.getSentence("Ala ma lisa i psa");
        } catch (Exception ex) {
            isExcetpion = true;

        }

        Assert.assertEquals( isExcetpion, true);
    }

    @org.junit.Test
    public void checkAddPawel3() {
        duplikcatInterface.addSentence("Pawel ma kota");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(),4 );
    }

    @org.junit.Test
    public void removeSentence5() {

        Boolean isExcetpion = false;
        duplikcatInterface.removeSentence("Ala ma kota");

        try{

            duplikcatInterface.getSentence("ma kota");
        } catch (Exception ex) {
            isExcetpion = true;
        }

        Assert.assertEquals(isExcetpion, true);

    }

    @org.junit.Test
    public void removeSentence5a() {
        duplikcatInterface.removeSentence("Ala ma kota");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(), 4);

    }

    @org.junit.Test
    public void removeSentencePawel5b() {
        duplikcatInterface.removeSentence("Pawel ma kota");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(), 1);

    }

    @org.junit.Test
    public void getDictionaryv16() {
        Boolean checkIfExist = duplikcatInterface.getDictionary().containsValue("ma kota");
        Assert.assertEquals(checkIfExist, true);
    }

    @org.junit.Test
    public void getDictionary7() {
        Boolean checkIfExist = duplikcatInterface.getDictionary().containsValue("Pawel");
        Assert.assertEquals(checkIfExist, true);
    }

    @org.junit.Test
    public void addSentenceMore8() {
        duplikcatInterface.addSentence("Pawel idze do sklepu !!!");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(), 6);
    }

    @org.junit.Test
    public void exceptionNow9() {

        Boolean isExcetpion = false;

        try{

            duplikcatInterface.getSentence("Pawel idzie do sklepu!");
        } catch (Exception ex) {
            isExcetpion = true;
        }
        Assert.assertEquals(isExcetpion, true);
    }

    @org.junit.Test
    public void addSentenceMicroMake() {
        duplikcatInterface.addSentence("Pawel idze do sklepu !!!");
        Assert.assertEquals(duplikcatInterface.getDictionary().size(), 6);
    }

}
